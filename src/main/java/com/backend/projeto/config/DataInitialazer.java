package com.backend.projeto.config;

import com.backend.projeto.entity.Role;
import com.backend.projeto.entity.User;
import com.backend.projeto.repository.RoleRepository;
import com.backend.projeto.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DataInitialazer implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        List<User> users = userRepository.findAll();

        Role roleObj1 = new Role();
        roleObj1.setName("ROLE_ADMIN");

        Role roleObj2 = new Role();
        roleObj2.setName("ROLE_ALUNO");

        roleRepository.save(roleObj1);
        roleRepository.save(roleObj2);

        if (users.isEmpty()) {
            createUsers("Jairo", "jns@email.com", encoder.encode("123"), roleObj1);
            createUsers("Jairo Filho", "jnsf@email.com", encoder.encode("123"), roleObj2);
            createUsers("Silvana Luz", "sls@email.com", encoder.encode("123"), roleObj2);
            createUsers("Caio Sousa", "cls@email.com", encoder.encode("123"), roleObj2);
        }

    }

    private void createUsers(String name, String email, String password, Role role) {
        User user = new User(name, email, password, Arrays.asList(role));
        userRepository.save(user);
    }
}
